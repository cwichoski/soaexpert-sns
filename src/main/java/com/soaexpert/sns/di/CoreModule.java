package com.soaexpert.sns.di;

import br.com.ingenieux.cloudy.awseb.di.BaseAWSModule;

import com.google.inject.AbstractModule;

public class CoreModule extends AbstractModule {
    @Override
    protected void configure() {
        install(new BaseAWSModule().withDynamicRegion());
    }
}
