package com.soaexpert.sns;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import javax.servlet.ServletRegistration;

import org.jolokia.http.AgentServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.JmxReporter;
import com.hubspot.dropwizard.guice.GuiceBundle;
import com.soaexpert.sns.di.CoreModule;

public class Main extends Application<SNSServiceConfiguration> {
	final Logger logger = LoggerFactory.getLogger(Main.class);

	public static void main(String[] args) throws Exception {
		new Main().run(args);
	}

	@Override
	public String getName() {
		return "sns-service";
	}

	@Override
	public void initialize(Bootstrap<SNSServiceConfiguration> bootstrap) {
		final GuiceBundle.Builder<SNSServiceConfiguration> configBuilder = GuiceBundle
				.<SNSServiceConfiguration> newBuilder()
				.addModule(new CoreModule())
				.enableAutoConfig("com.soaexpert.sns")
				.setConfigClass(SNSServiceConfiguration.class);

		GuiceBundle<SNSServiceConfiguration> guiceBundle = configBuilder
				.build();

		bootstrap.addBundle(guiceBundle);
	}

	@Override
	public void run(SNSServiceConfiguration cfg, Environment env)
			throws Exception {
		JmxReporter.forRegistry(env.metrics()).build().start();

		ServletRegistration.Dynamic jolokia = env.servlets().addServlet(
				"jolokia", AgentServlet.class);

		jolokia.addMapping("/jolokia/*");
	}
}
